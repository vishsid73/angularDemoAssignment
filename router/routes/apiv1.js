var express = require('express');
var router = express.Router();


var fs = require('fs');
var path = require('path');
var crypto = require('crypto');
var debug = require('debug');

//CONTROLLERS
var toll = require( '../../controllers/toll.controller');


//PATH
router.get('/',     function(req,res, next){
    res.json ('Hello!, Welcome to Trip In Toll API V1.');
});

router.get('/locations/:locid?', toll.getAllLocation);
router.post('/locations', toll.createLocation);
router.put('/locations/:locid', toll.updateLocation);
router.delete('/locations/:locid', toll.deleteLocation);


module.exports = router;