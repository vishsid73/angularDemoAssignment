/**
 * Created by LexStart on 01/07/16.
 */
var _ = require("underscore");
var moment = require("moment");
var Q = require('q');
var async = require('async');

var express = require('express');

mongoose = require('mongoose'); //mongo connection
mongoose.Promise = require('q').Promise;
var db = require('../mongo_models/db');
var toll = require('../mongo_models/toll');


module.exports = {
    getToll: function(operation, object){
        if(operation == 'all'){
            return mongoose.model('Toll').find({}).exec();
        }
        else if(operation == 'one'){
            return mongoose.model('Toll').findOne(object).exec();
        }

    },
    createToll: function(Toll){
        var deferred = Q.defer();

        if(Toll == null){
            throw new Error("No Toll");
        }

        var newItem = new toll(Toll);
        newItem.save(function(err, result) {
            if (err)
                deferred.reject(err);
            deferred.resolve();
        });

        return deferred.promise;
    },
    updateToll: function(Toll){
        var deferred = Q.defer();

        if(Toll == null){
            throw new Error("No Toll");
        }

        mongoose.model('Toll')
            .findOne({id: Toll.id})
            .exec()
            .then(function(result){
                result.name = Toll.name;
                result.description = Toll.description;
                result.url = Toll.url;
                result.save(function(err, result) {
                    if (err)
                        deferred.reject(err);
                    deferred.resolve(result);
                });
            });
        
        return deferred.promise;
    },
    deleteToll: function(Tollid){
        return mongoose.model('Toll').deleteOne({id: Tollid});
    },
};