Vehicle_Types = {
    0 : 'CarRate_single', //car/jeep
    1 : 'LCVRate_single', //lcv
    2 : 'BusRate_multi', //bus/truck
    3 : 'MultiAxleRate_single', //3 axel
    4 : 'FourToSixExel_Single', //4-6 axel
    5 : 'SevenOrmoreExel_Single', //7 more
    6 : 'HCM_EME_Single', //HME
};

module.exports = Vehicle_Types;