var _ = require("underscore");
var Q = require('q');
var async = require('async');
var request = require('request');
var rp = require('request-promise');
var moment = require('moment');

var express = require('express');
var vehicle_type = require('../constants/Vehicle_Types');

mongoose = require('mongoose'); //mongo connection
mongoose.Promise = require('q').Promise;

var toll_dao = require('../dao/toll.dao');


module.exports = {

    getAllLocation: function(req, res, next){
        if(!req.query.locid){
            return toll_dao.getToll('all')
                .then(function(result){
                    return res.status(200).json({status: 'success', location: result, length: result.length});
                })
                .catch(function(error){
                    return res.status(400).json({status: 'error', error: error.message});
                });
        }
        else{
            return toll_dao.getToll('one', {"id": req.query.locid})
                .then(function(result){
                    return res.status(200).json({status: 'success', toll: result, length: result.length});
                })
                .catch(function(error){
                    return res.status(400).json({status: 'error', error: error.message});
                });
        }
    },
    getLocation: function(req, res, next){
        return toll_dao.getToll('one', {"latitude": 26.386389, "longitude": 80.021485})
            .then(function(result){
                return res.status(200).json({status: 'success', toll: result, length: result.length});
            })
            .catch(function(error){
                return res.status(400).json({status: 'error', error: error.message});
            });
    },
    createLocation: function(req, res, next){
        req.body.location.url = req.body.location.name;
        return toll_dao.createToll(req.body.location)
            .then(function(result){
                return res.status(201).json({status: 'success'});
            })
            .catch(function(error){
                return res.status(400).json({status: 'error', error: error.message});
            });
    },
    updateLocation: function(req, res, next){
        req.body.location.id = req.query.locid;
        return toll_dao.updateToll(req.body.location)
            .then(function(result){
                return res.status(200).json({status: 'success'});
            })
            .catch(function(error){
                return res.status(400).json({status: 'error', error: error.message});
            });
    },
    deleteLocation: function(req, res, next){
        return toll_dao.deleteToll(req.query.tollid)
            .then(function(result){
                return res.status(201).json({status: 'success'});
            })
            .catch(function(error){
                return res.status(400).json({status: 'error', error: error.message});
            });
    },
    

};