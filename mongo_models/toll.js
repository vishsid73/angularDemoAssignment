
var mongoose = require('mongoose');

var tollSchema = new mongoose.Schema({
    name: String,
    url: String,
    description: String,
    latitude: Number,
    longitude: Number,
    date: { type: Date, default: Date.now }
});
var Toll = mongoose.model('Toll', tollSchema);

// make this available to our users in our Node applications
module.exports = Toll;
